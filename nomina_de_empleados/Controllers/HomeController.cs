﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace nomina_de_empleados.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Aplicacion nomina de empleados para Silogik";
           
            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Contacto - Maximiliano Elias Alvarez";

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
